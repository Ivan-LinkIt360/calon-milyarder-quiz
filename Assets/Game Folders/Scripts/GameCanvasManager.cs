using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameCanvasManager : CanvasManager
{
    private void OnEnable()
    {
        if (GameManager.Instance == null)
        {
            Debug.Log("nope");
            return;
        }
        GameManager.Instance.OnStateChange += Instance_OnStateChange;

        GameManager.Instance.ChangeState(GameState.Loading);
    }

    private void OnDisable()
    {
        GameManager.Instance.OnStateChange -= Instance_OnStateChange;
    }

    private void Instance_OnStateChange(GameState state)
    {
        switch (state)
        {
            case GameState.Game:
                SetPage(PageName.Game);
                break;
            case GameState.Loading:
                SetPage(PageName.Loading);
                AdsManager.Instance.RequestBanner();
                break;
            case GameState.ListSoal:
                SetPage(PageName.ListSoal);
                break;
            case GameState.Result:
                SetPage(PageName.Result);
                break;
        }
    }
}
