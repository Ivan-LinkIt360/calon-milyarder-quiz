﻿public enum PageName
{
    Menu,
    Level,
    Petunjuk,
    Shop,
    Pengaturan,
    Game,
    Loading,
    ListSoal,
    Result
}