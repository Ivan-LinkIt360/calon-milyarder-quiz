using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ListSoalPage : Page
{
    [SerializeField] private Button b_home;
    [SerializeField] private TMP_Text label_bottom;
    [SerializeField] private TMP_Text label_top;
    [SerializeField] private QuizManager manager;

    [SerializeField] private Button[] allButtonLevels;

    private void Start()
    {
        b_home.onClick.AddListener(() => PindahScene("Main Menu"));

        foreach (var item in allButtonLevels)
        {
            item.interactable = false;
        }

        for (int i = 0; i < manager.GetCurrentNomor() + 1; i++)
        {
            allButtonLevels[i].interactable = true;
        }

        label_bottom.text = GameManager.Instance.GetCurrentLevelSelected().ToString();
        label_top.text = GameManager.Instance.GetCurrentLevelSelected().ToString();
    }
    public void SetupSoal(int nomor)
    {
        GameManager.Instance.ChangeState(GameState.Game);
        manager.ShowSoal(nomor);
    }
}
