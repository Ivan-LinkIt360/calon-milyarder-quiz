﻿[System.Serializable]
public class Soal
{
    public string pertanyaan;
    public string[] opsis;

    public int jawaban;

    public Soal(string pertanyaan , string[] opsi , int j)
    {
        this.pertanyaan = pertanyaan;
        this.opsis = opsi;
        jawaban = j;
    }
}