using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CoinFrame : MonoBehaviour
{
    [SerializeField] private Button b_shop;
    [SerializeField] private TMP_Text label_coin;

    private void OnEnable()
    {
        GameManager.Instance.OnCoinChange += Instance_OnCoinChange;
    }

    private void OnDisable()
    {
        GameManager.Instance.OnCoinChange -= Instance_OnCoinChange;
    }

    private void Instance_OnCoinChange(int currentCoin)
    {
        label_coin.text = $"{currentCoin}";
    }

    private void Start()
    {
        b_shop.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Shop));
        label_coin.text = $"{GameManager.Instance.GetCoin()}";
    }
}
