using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectTurn : MonoBehaviour
{
    [SerializeField, Range(50, 150f)] private float turnSpeed;

    private void Update()
    {
        transform.Rotate(0f, Time.deltaTime * turnSpeed, 0f);
    }
}
