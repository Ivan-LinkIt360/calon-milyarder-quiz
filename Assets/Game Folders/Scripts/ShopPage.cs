using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ShopPage : Page
{
    [SerializeField] private GameObject panel_loading;
    [SerializeField] private Button b_home;

    [SerializeField] private Button b_real;
    [SerializeField] private Button b_inGame;

    [SerializeField] private GameObject[] panels;

    [SerializeField] private Button[] buttonsInGames;

    private void Start()
    {
        b_home.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Menu));

        b_real.onClick.AddListener(() => SetShopPanel(false));
        b_inGame.onClick.AddListener(() => SetShopPanel(true));

        buttonsInGames[0].onClick.AddListener(() => BuyInGame(InGameShop.levelBlockchain));
        buttonsInGames[1].onClick.AddListener(() => BuyInGame(InGameShop.levelPendidikan));
        buttonsInGames[2].onClick.AddListener(() => BuyInGame(InGameShop.levelMistery));
        buttonsInGames[3].onClick.AddListener(() => BuyInGame(InGameShop.jawabanBenar));
        buttonsInGames[4].onClick.AddListener(() => BuyInGame(InGameShop.fiftyFifty));
    }

    private void SetShopPanel(bool inGame)
    {
        panels[0].SetActive(inGame);
        panels[1].SetActive(!inGame);
    }

    public void ProcessBuy()
    {
        panel_loading.SetActive(true);
        TMP_Text label = panel_loading.GetComponentInChildren<TMP_Text>();
        label.text = "Processing ..";

        StartCoroutine(HideLoading());
    }

    public void BuyCoin(string shopId)
    {
        switch (shopId)
        {
            case "coin5K":
                GameManager.Instance.AddCoin(5000);
                break;
            case "coin10K":
                GameManager.Instance.AddCoin(10000);
                break;
            case "coin30K":
                GameManager.Instance.AddCoin(30000);
                break;
            case "coin100K":
                GameManager.Instance.AddCoin(100000);
                break;
            case "coin300K":
                GameManager.Instance.AddCoin(300000);
                break;
            case "removeAds":
                AdsManager.Instance.RemoveAds();
                break;
            case "donate":
                break;
        }

        StopAllCoroutines();
        panel_loading.SetActive(false);
    }

    public void BuyInGame(InGameShop item)
    {
        switch (item)
        {
            case InGameShop.levelBlockchain:
                if(GameManager.Instance.GetCoin() >= 50000)
                {
                    //unlock level blockchain
                }
                else
                {
                    panel_loading.SetActive(true);
                    TMP_Text label = panel_loading.GetComponentInChildren<TMP_Text>();
                    label.text = "Coin Kurang!!";
                }
                break;
            case InGameShop.levelPendidikan:
                if (GameManager.Instance.GetCoin() >= 50000)
                {
                    //unlock level blockchain
                }
                else
                {
                    panel_loading.SetActive(true);
                    TMP_Text label = panel_loading.GetComponentInChildren<TMP_Text>();
                    label.text = "Coin Kurang!!";
                }
                break;
            case InGameShop.levelMistery:
                if (GameManager.Instance.GetCoin() >= 50000)
                {
                    //unlock level blockchain
                }
                else
                {
                    panel_loading.SetActive(true);
                    TMP_Text label = panel_loading.GetComponentInChildren<TMP_Text>();
                    label.text = "Coin Kurang!!";
                }
                break;
            case InGameShop.jawabanBenar:
                if (GameManager.Instance.GetCoin() >= 100000)
                {
                    GameManager.Instance.BuySkill(Skill.JawabanBenar);
                }
                else
                {
                    panel_loading.SetActive(true);
                    TMP_Text label = panel_loading.GetComponentInChildren<TMP_Text>();
                    label.text = "Coin Kurang!!";
                }
                break;
            case InGameShop.fiftyFifty:
                if (GameManager.Instance.GetCoin() >= 100000)
                {
                    GameManager.Instance.BuySkill(Skill.FiftyFifty);
                }
                else
                {
                    panel_loading.SetActive(true);
                    TMP_Text label = panel_loading.GetComponentInChildren<TMP_Text>();
                    label.text = "Coin Kurang!!";
                }
                break;
        }
        StartCoroutine(HideLoading());
    }

    public void BuyFailed()
    {
        panel_loading.SetActive(true);
        TMP_Text label = panel_loading.GetComponentInChildren<TMP_Text>();
        label.text = "Purchasing Failed!!";

        StartCoroutine(HideLoading());
    }

    IEnumerator HideLoading()
    {
        yield return new WaitForSeconds(5f);
        panel_loading.gameObject.SetActive(false);
    }
}



public enum InGameShop 
{
    levelBlockchain,
    levelPendidikan,
    levelMistery,
    jawabanBenar,
    fiftyFifty
}