using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasManager : MonoBehaviour
{
    [SerializeField] private Page[] allPages;

    public void SetPage(PageName namaPage)
    {
        foreach (var item in allPages)
        {
            item.gameObject.SetActive(false);
        }

        int cariIndex = Array.FindIndex(allPages, p => p.nama == namaPage);
        allPages[cariIndex].gameObject.SetActive(true);
    }
}
