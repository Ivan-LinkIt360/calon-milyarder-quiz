using System;
using System.Collections;
using System.Collections.Generic;
using Firebase.Database;
using Firebase.Extensions;
using UnityEngine;
using UnityEngine.SceneManagement;

public class QuizManager : MonoBehaviour
{
    [SerializeField] private FirebaseDatabase database;
    [SerializeField] private DatabaseReference reference;

    [SerializeField] private DaftarSoal currentDaftarSoal;

    [SerializeField] private int nomor;

    public delegate void ListSoalDelegate(DaftarSoal daftar);
    public event ListSoalDelegate OnDaftarSoalChange;

    public delegate void SoalDelegate(Soal newSoal);
    public event SoalDelegate OnSoalChange;

    public delegate void GameResultDelegate(bool menang);
    public event GameResultDelegate OnResultUpdate;

    private void Start()
    {
        database = FirebaseDatabase.DefaultInstance;
        reference = database.RootReference;

        LoadSoal(GameManager.Instance.GetCurrentLevelSelected());
    }

    private void LoadSoal(GameLevel selectedLevel)
    {
        database.GetReference(selectedLevel.ToString()).GetValueAsync().ContinueWithOnMainThread(task => {
          if (task.IsFaulted)
          {
                // Handle the error...
                Debug.Log("Soal not found!");

                SceneManager.LoadScene(0);
            }
          else if (task.IsCompleted)
          {
                DataSnapshot snapshot = task.Result;
                Debug.Log($"find soal : {selectedLevel}. database cari yang ketemu : {snapshot.GetRawJsonValue()}");

                DaftarSoal thisSoal = JsonUtility.FromJson<DaftarSoal>(snapshot.GetRawJsonValue());
                currentDaftarSoal = thisSoal;

                SetupSoal();
          }
      });
    }

    public void JawabanBenar(bool benar)
    {
        if(benar)
        {
            nomor++;
        }

        GameManager.Instance.ChangeState(GameState.Result);
        OnResultUpdate?.Invoke(benar);
    }

    private void SetupSoal()
    {
        Resuffle(currentDaftarSoal.semuaSoal);
        GameManager.Instance.ChangeState(GameState.ListSoal);

        OnDaftarSoalChange?.Invoke(currentDaftarSoal);
        //ShowSoal(nomor);
    }

    public void NextSoal()
    {
        if(nomor < 15)
        {
            GameManager.Instance.ChangeState(GameState.Game);
            ShowSoal(nomor);
        }
    }

    public void ShowSoal(int n)
    {
        nomor = n;
        OnSoalChange?.Invoke(currentDaftarSoal.semuaSoal[nomor]);
    }

    void Resuffle(Soal[] currentSoals)
    {
        // Knuth shuffle algorithm :: courtesy of Wikipedia :)
        for (int t = 0; t < currentSoals.Length; t++)
        {
            Soal tmp = currentSoals[t];
            int r = UnityEngine.Random.Range(t, currentSoals.Length);
            currentSoals[t] = currentSoals[r];
            currentSoals[r] = tmp;
        }
    }

    public int GetCurrentNomor()
    {
        return nomor;
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.L))
        {
            Debug.Log("Create template soal di database!!");
            CreateAllSoal();
        }
    }

    public void CreateAllSoal()
    {
        List<Soal> daftarSoal = new List<Soal>();
        string[] temp =
        {
            "test opsi A",
            "test opsi B",
            "test opsi C",
            "test opsi D",

        };
        for (int i = 0; i < 15; i++)
        {
            Soal baru = new Soal($"Soal Nomor {i + 1}", temp, 0);
            daftarSoal.Add(baru);
        }
        DaftarSoal soalUmum = new DaftarSoal(daftarSoal.ToArray());
        string json = JsonUtility.ToJson(soalUmum);

        if (reference == null)
        {
            reference = FirebaseDatabase.DefaultInstance.RootReference;
        }

        reference.Child(GameLevel.Common.ToString()).SetRawJsonValueAsync(json);
        reference.Child(GameLevel.Indonesian.ToString()).SetRawJsonValueAsync(json);
        reference.Child(GameLevel.Mistery.ToString()).SetRawJsonValueAsync(json);
        reference.Child(GameLevel.History.ToString()).SetRawJsonValueAsync(json);
    }

    public void SetActiveSkill()
    {

    }
}

public enum Skill
{
    FiftyFifty,
    JawabanBenar
}