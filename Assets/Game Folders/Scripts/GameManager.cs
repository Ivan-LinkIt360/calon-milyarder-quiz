using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    [SerializeField] private int coin;
    [SerializeField] public SkillData[] skills;

    [SerializeField] private GameState currentState;
    [SerializeField] private GameLevel currentLevel;

    public delegate void GameStateDelegate(GameState state);
    public event GameStateDelegate OnStateChange;

    public delegate void CoinStateDelegate(int currentCoin);
    public event CoinStateDelegate OnCoinChange;
    
    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);

        LoadData();
    }

    private void LoadData()
    {
        if (PlayerPrefs.HasKey("COIN"))
        {
            coin = PlayerPrefs.GetInt("COIN");
        }

        for (int i = 0; i < skills.Length; i++)
        {
            if(PlayerPrefs.HasKey(skills[i].skillName.ToString()))
            {
                skills[i].total = PlayerPrefs.GetInt(skills[i].skillName.ToString());
            }
        }
    }

    public GameLevel GetCurrentLevelSelected()
    {
        return currentLevel;
    }

    internal void SetLevel(GameLevel level)
    {
        currentLevel = level;
    }

    public void ChangeState(GameState newState)
    {
        currentState = newState;

        OnStateChange?.Invoke(currentState);
    }

    public void AddCoin(int total)
    {
        coin += total;
        PlayerPrefs.SetInt("COIN", coin);

        OnCoinChange?.Invoke(coin);
    }

    public int GetCoin()
    {
        return coin;
    }

    public void UseSkill(Skill skill)
    {
        switch (skill)
        {
            case Skill.FiftyFifty:
                skills[0].total--;
                PlayerPrefs.SetInt(skills[0].skillName.ToString(), skills[0].total);
                break;
            case Skill.JawabanBenar:
                skills[1].total--;
                PlayerPrefs.SetInt(skills[1].skillName.ToString(), skills[1].total);
                break;
        }
    }

    public void BuySkill(Skill skill)
    {
        switch (skill)
        {
            case Skill.FiftyFifty:
                skills[0].total++;
                PlayerPrefs.SetInt(skills[0].skillName.ToString(), skills[0].total);
                break;
            case Skill.JawabanBenar:
                skills[1].total++;
                PlayerPrefs.SetInt(skills[1].skillName.ToString(), skills[1].total);
                break;
        }
    }

    public int GetTotalSkill(Skill skill)
    {
        int n = 0;
        SkillData tempSkill = Array.Find(skills, s => s.skillName == skill);
        n = tempSkill.total;

        return n;
    }
}

public enum GameState
{
    Menu,
    Level,
    Petunjuk,
    Shop,
    Pengaturan,
    Game,
    Loading,
    ListSoal,
    Result
}

public enum GameLevel
{
    Common,
    Indonesian,
    History,
    Mistery
}

[System.Serializable]
public class SkillData
{
    public Skill skillName;
    public int total;

    public void UseSkill()
    {
        total--;
        PlayerPrefs.SetInt(skillName.ToString(), total);
    }
}
