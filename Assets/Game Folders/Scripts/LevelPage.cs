﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class LevelPage : Page
{
    [SerializeField] private Button b_home;

    [SerializeField] private Button b_level1;
    [SerializeField] private Button b_level2;
    [SerializeField] private Button b_level3;
    [SerializeField] private Button b_level4;

    [SerializeField] private GameObject panel_haveToBuy;
    [SerializeField] private Button b_cancel;

    [SerializeField] private string namaScene;

    private void Start()
    {
        b_home.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Menu));

        b_level1.onClick.AddListener(() => {
            GameManager.Instance.SetLevel(GameLevel.Common);
            SetLevel(0);
        });

        b_level2.onClick.AddListener(() => {
            GameManager.Instance.SetLevel(GameLevel.Indonesian);
            SetLevel(1);
        });

        b_level3.onClick.AddListener(() => {
            GameManager.Instance.SetLevel(GameLevel.Mistery);
            SetLevel(2);
        });

        b_level4.onClick.AddListener(() => {
            GameManager.Instance.SetLevel(GameLevel.History);
            SetLevel(3);
        });

        b_cancel.onClick.AddListener(() => panel_haveToBuy.SetActive(false));
    }

    private void SetLevel(int number)
    {
        if(number == 0 || number == 1)
        {
            
        }
        else
        {
            panel_haveToBuy.SetActive(true);
            return;
        }

        AdsManager.Instance.RequestInterstitial();
        PindahScene(namaScene);
    }

    public void Pindah()
    {
        PindahScene(namaScene);
    }
}

