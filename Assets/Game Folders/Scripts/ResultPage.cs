using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ResultPage : Page
{
    [SerializeField] Button b_home;
    [SerializeField] Button b_next;
    [SerializeField] Button b_allSoal;

    [SerializeField] private TMP_Text label_result;
    [SerializeField] private TMP_Text label_subResult;

    private bool kalah;

    [SerializeField] private QuizManager manager;

    private void OnEnable()
    {
        manager.OnResultUpdate += Manager_OnResultUpdate;
    }

    private void OnDisable()
    {
        manager.OnResultUpdate -= Manager_OnResultUpdate;    
    }

    private void Manager_OnResultUpdate(bool menang)
    {
        SetResult(menang);
        if(manager.GetCurrentNomor() == 15)
        {
            b_next.gameObject.SetActive(false);
        }
    }

    private void Start()
    {
        b_home.onClick.AddListener(() =>
        {
            AdsManager.Instance.RequestInterstitial();
            AudioManager.Instance.HentikanSuara("Win");
            AudioManager.Instance.HentikanSuara("Lose");
            PindahScene("Main Menu"); 
        });

        b_next.onClick.AddListener(() =>
        {
            AudioManager.Instance.HentikanSuara("Win");
            AudioManager.Instance.HentikanSuara("Lose");
            manager.NextSoal();
        });

        b_allSoal.onClick.AddListener(() =>
        {
            AudioManager.Instance.HentikanSuara("Win");
            AudioManager.Instance.HentikanSuara("Lose");
            AdsManager.Instance.RequestInterstitial();
            
            GameManager.Instance.ChangeState(GameState.ListSoal); 
        });
    }

    public void SetResult(bool menang)
    {
        kalah = !menang;
        if(menang)
        {
            AudioManager.Instance.MainkanSuara("Win");
            label_result.text = $"<color=green>CORRECT !!";
            label_subResult.text = $"Congratulation you get ${manager.GetCurrentNomor()},000,000,000";
            b_next.gameObject.SetActive(true);
        }
        else
        {
            AudioManager.Instance.MainkanSuara("Lose");
            label_result.text = $"<color=red>WRONG !!";
            label_subResult.text = "Better luck next time . . ";
            b_next.gameObject.SetActive(false);
        }
    }
}
