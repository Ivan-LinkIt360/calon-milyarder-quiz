﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Page : MonoBehaviour
{
    public PageName nama;

    public void PindahScene(string namaScene)
    {
        SceneManager.LoadScene(namaScene);
    }
}