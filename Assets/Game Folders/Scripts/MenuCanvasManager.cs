using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuCanvasManager : CanvasManager
{
    private void OnEnable()
    {
        if(GameManager.Instance == null)
        {
            Debug.Log("nope");
            return;
        }
        GameManager.Instance.OnStateChange += Instance_OnStateChange;

        switch (Application.internetReachability)
        {
            case NetworkReachability.NotReachable:
                GameManager.Instance.ChangeState(GameState.Loading);
                break;
            case NetworkReachability.ReachableViaCarrierDataNetwork:
                GameManager.Instance.ChangeState(GameState.Menu);
                break;
            case NetworkReachability.ReachableViaLocalAreaNetwork:
                GameManager.Instance.ChangeState(GameState.Menu);
                break;
        }
    }

    private void OnDisable()
    {
        GameManager.Instance.OnStateChange -= Instance_OnStateChange;
    }

    private void Instance_OnStateChange(GameState state)
    {
        switch (state)
        {
            case GameState.Menu:
                SetPage(PageName.Menu);
                break;
            case GameState.Level:
                SetPage(PageName.Level);
                break;
            case GameState.Petunjuk:
                SetPage(PageName.Petunjuk);
                break;
            case GameState.Pengaturan:
                SetPage(PageName.Pengaturan);
                break;
            case GameState.Shop:
                SetPage(PageName.Shop);
                break;
        }
    }

    private void Start()
    {
        SetPage(PageName.Menu);

        GameManager.Instance.OnStateChange += Instance_OnStateChange;
    }
}
