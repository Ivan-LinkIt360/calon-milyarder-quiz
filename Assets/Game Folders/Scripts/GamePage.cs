using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GamePage : Page
{
    [SerializeField] private Button b_home;

    [SerializeField] private TMP_Text label_title;
    [SerializeField] private TMP_Text label_titleBelow;

    [SerializeField] private TMP_Text label_timer;
    [SerializeField] private TMP_Text label_hadiah;

    [SerializeField] private TMP_Text label_soal;
    [SerializeField] private TMP_Text[] label_opsi;

    [SerializeField] private Button opsiA;
    [SerializeField] private Button opsiB;
    [SerializeField] private Button opsiC;
    [SerializeField] private Button opsiD;

    [SerializeField] private int currentJawaban = 0;

    [SerializeField] private Button b_limaPuluh;
    [SerializeField] private Button b_jawaban;

    [SerializeField] private TMP_Text label_limaPuluh;
    [SerializeField] private TMP_Text label_jawaban;

    [SerializeField] private GameObject label_warning;

    [SerializeField] private Sprite[] buttonSkins;

    [SerializeField] QuizManager manager;

    private void OnEnable()
    {
        manager.OnSoalChange += Manager_OnSoalChange;
    }
    private void OnDisable()
    {
        manager.OnSoalChange -= Manager_OnSoalChange;
    }

    private void Start()
    {
        b_home.onClick.AddListener(() => 
        {
            AdsManager.Instance.RequestInterstitial();
            PindahScene("Main Menu");
        });

        opsiA.onClick.AddListener(() => CheckJawaban(0));
        opsiB.onClick.AddListener(() => CheckJawaban(1));
        opsiC.onClick.AddListener(() => CheckJawaban(2));
        opsiD.onClick.AddListener(() => CheckJawaban(3));

        b_jawaban.onClick.AddListener(() =>
        {
            List<Button> allOpsi = new List<Button>();
            allOpsi.Add(opsiA);
            allOpsi.Add(opsiB);
            allOpsi.Add(opsiC);
            allOpsi.Add(opsiD);

            foreach (var item in allOpsi)
            {
                item.GetComponent<Image>().sprite = buttonSkins[0];
                item.interactable = false;
            }
            allOpsi[currentJawaban].interactable = true;
            allOpsi[currentJawaban].GetComponent<Image>().sprite = buttonSkins[1];

            GameManager.Instance.skills[1].UseSkill();
        });

        b_limaPuluh.onClick.AddListener(() =>
        {
            List<Button> allOpsi = new List<Button>();
            allOpsi.Add(opsiA);
            allOpsi.Add(opsiB);
            allOpsi.Add(opsiC);
            allOpsi.Add(opsiD);

            foreach (var item in allOpsi)
            {
                item.GetComponent<Image>().sprite = buttonSkins[0];
                item.interactable = false;
            }
            int tempNomor = currentJawaban + 1;
            if(tempNomor > 3)
            {
                tempNomor = 0;
            }
            allOpsi[currentJawaban].interactable = true;
            allOpsi[currentJawaban].GetComponent<Image>().sprite = buttonSkins[1];
            allOpsi[tempNomor].interactable = true;
            allOpsi[tempNomor].GetComponent<Image>().sprite = buttonSkins[1];

            GameManager.Instance.skills[0].UseSkill();
        });
    }

    IEnumerator HilangkanWarning()
    {
        yield return new WaitForSeconds(2f);
        label_warning.SetActive(false);
    }

    private void CheckJawaban(int n)
    {
        if(n == currentJawaban)
        {
            manager.JawabanBenar(true);
        }
        else
        {
            manager.JawabanBenar(false);
        }

        StopAllCoroutines();
    }

    private void Manager_OnSoalChange(Soal newSoal)
    {
        label_limaPuluh.text = GameManager.Instance.GetTotalSkill(Skill.FiftyFifty).ToString();
        label_jawaban.text = GameManager.Instance.GetTotalSkill(Skill.JawabanBenar).ToString();

        b_limaPuluh.interactable = GameManager.Instance.GetTotalSkill(Skill.FiftyFifty) >= 1;
        b_jawaban.interactable = GameManager.Instance.GetTotalSkill(Skill.JawabanBenar) >= 1;



        List<Button> allOpsi = new List<Button>();
        allOpsi.Add(opsiA);
        allOpsi.Add(opsiB);
        allOpsi.Add(opsiC);
        allOpsi.Add(opsiD);

        foreach (var item in allOpsi)
        {
            item.GetComponent<Image>().sprite = buttonSkins[0];
            item.interactable = true;
        }

        label_hadiah.text = $"${manager.GetCurrentNomor() + 1},000,000,000";
        label_title.text = $"{GameManager.Instance.GetCurrentLevelSelected()} {manager.GetCurrentNomor() + 1}";
        label_titleBelow.text = $"{GameManager.Instance.GetCurrentLevelSelected()}";
        label_soal.text = newSoal.pertanyaan;

        for (int i = 0; i < 4; i++)
        {
            string temp = "A";
            if (i == 0)
            {
                temp = "A";
            }if (i == 1)
            {
                temp = "B";
            }if (i == 2)
            {
                temp = "C";
            }if (i == 3)
            {
                temp = "D";
            }
            label_opsi[i].text = $"<color=green>{temp} :</color> {newSoal.opsis[i]}";
        }

        StartCoroutine(MulaiTimer());
    }

    IEnumerator MulaiTimer()
    {
        int timer = 20;
        for (int i = 0; i < 20; i++)
        {
            label_timer.text = timer.ToString();
            yield return new WaitForSeconds(1f);
            timer--;
        }

        manager.JawabanBenar(false);
    }
}
