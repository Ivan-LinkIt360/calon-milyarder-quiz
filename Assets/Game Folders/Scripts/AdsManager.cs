using GoogleMobileAds.Api;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdsManager : MonoBehaviour
{
    public static AdsManager Instance;

    //[SerializeField] private string keyApp = "18a7cb17d";
    //[SerializeField] private IronSourceBannerPosition bannerPosition = IronSourceBannerPosition.TOP;
    [SerializeField] private bool isTest = true;

    [SerializeField] private string bannerID;
    [SerializeField] private string interstitialID;
    [SerializeField] private string rewardedID;

    [SerializeField] private string bannerTest;
    [SerializeField] private string interstitialTest;
    [SerializeField] private string rewardedTest;

    [SerializeField] private string bannerAdmob;
    [SerializeField] private string interstitialAdmob;
    [SerializeField] private string rewardedAdmob;

    private BannerView _bannerView;
    private InterstitialAd _interstitialAd;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);

        MobileAds.Initialize(onInitStatus);
    }

    private void onInitStatus(InitializationStatus status)
    {
        if(isTest)
        {
            bannerID = bannerTest;
            interstitialID = interstitialTest;
            rewardedID = rewardedTest;
        }
        else
        {
            bannerID = bannerAdmob;
            interstitialID = interstitialAdmob;
            rewardedID = rewardedAdmob;
        }
        RequestBanner();
    }

    private void Start()
    {
        if (PlayerPrefs.HasKey("ADS"))
        {
            return;
        }

        /*IronSource.Agent.validateIntegration();
        IronSource.Agent.init(keyApp);*/  
    }

    private void OnEnable()
    {
        /*IronSourceEvents.onSdkInitializationCompletedEvent += IronSourceEvents_onSdkInitializationCompletedEvent;
        IronSourceRewardedVideoEvents.onAdRewardedEvent += RewardedVideoOnAdRewardedEvent;
        IronSourceInterstitialEvents.onAdClosedEvent += IronSourceInterstitialEvents_onAdClosedEvent;*/
    }

    private void OnDisable()
    {
        /*IronSourceEvents.onSdkInitializationCompletedEvent -= IronSourceEvents_onSdkInitializationCompletedEvent;
        IronSourceRewardedVideoEvents.onAdRewardedEvent -= RewardedVideoOnAdRewardedEvent;
        IronSourceInterstitialEvents.onAdClosedEvent -= IronSourceInterstitialEvents_onAdClosedEvent;*/
    }

    /*private void IronSourceEvents_onSdkInitializationCompletedEvent()
    {
        IronSource.Agent.loadRewardedVideo();
        IronSource.Agent.loadInterstitial();
    }*/

    /*private void RewardedVideoOnAdRewardedEvent(IronSourcePlacement placement, IronSourceAdInfo adInfo)
    {
        OnAdRewardSuccess?.Invoke(placement.getPlacementName());
    }*/

    /*private void IronSourceInterstitialEvents_onAdClosedEvent(IronSourceAdInfo info)
    {
        IronSource.Agent.loadInterstitial();
    }*/

    public void RequestBanner()
    {
        /*IronSource.Agent.loadBanner(IronSourceBannerSize.BANNER, bannerPosition);
        IronSource.Agent.displayBanner();*/

        // If we already have a banner, destroy the old one.
        if (_bannerView != null)
        {
            DestroyAd();
        }

        // Create a 320x50 banner at top of the screen
        _bannerView = new BannerView(bannerID, AdSize.Banner, AdPosition.Top);

        var adRequest = new AdRequest();
        adRequest.Keywords.Add("unity-admob-sample");

        // send the request to load the ad.
        _bannerView.LoadAd(adRequest);
    }

    private void DestroyAd()
    {
        if (_bannerView != null)
        {
            Debug.Log("Destroying banner ad.");
            _bannerView.Destroy();
            _bannerView = null;
        }
    }

    public void RequestRewardedAds(string placement)
    {
        /*if (IronSource.Agent.isRewardedVideoAvailable())
        {
            IronSource.Agent.showRewardedVideo(placement);
        }
        else
        {
            RequestRewardedAds(placement);
        }*/
    }

    public void RequestInterstitial()
    {
        /* if (IronSource.Agent.isInterstitialReady())
         {
             IronSource.Agent.showInterstitial();
         }
         else
         {
             IronSource.Agent.loadInterstitial();
             RequestInterstitial();
         }*/

        if (_interstitialAd != null)
        {
            _interstitialAd.Destroy();
            _interstitialAd = null;
        }

        Debug.Log("Loading the interstitial ad.");

        // create our request used to load the ad.
        var adRequest = new AdRequest();
        adRequest.Keywords.Add("unity-admob-sample");

        // send the request to load the ad.
        InterstitialAd.Load(interstitialID, adRequest,
            (InterstitialAd ad, LoadAdError error) =>
            {
              // if error is not null, the load request failed.
              if (error != null || ad == null)
                {
                    Debug.LogError("interstitial ad failed to load an ad " +
                                   "with error : " + error);
                    return;
                }

                Debug.Log("Interstitial ad loaded with response : "
                          + ad.GetResponseInfo());

                _interstitialAd = ad;
                _interstitialAd.Show();
            });
    }

    public void RemoveAds()
    {

    }
}
