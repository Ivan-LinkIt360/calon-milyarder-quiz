﻿using UnityEngine;
using UnityEngine.UI;

public class PengaturanPage : Page
{
    [SerializeField] private Button b_home;

    private void Start()
    {
        b_home.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Menu));
    }
}