﻿using UnityEngine;
using UnityEngine.UI;

public class MenuPage : Page
{
    [SerializeField] private Button b_mulai;
    [SerializeField] private Button b_shop;

    private void Start()
    {
        b_mulai.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Level));
        b_shop.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Shop));

        AdsManager.Instance.RequestBanner();
        AudioManager.Instance.MainkanSuara("BGM");
    }
}